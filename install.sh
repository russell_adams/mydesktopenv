#/bin/bash

cd ./i3
cp config ~/.i3/ 
cp i3status.conf ~/.i3status.conf

cd ../ && cd ./gnome-terminal-themes
for i in *.sh; do /bin/bash $i; done
